class CreateCollectionItems < ActiveRecord::Migration[5.2]
  def change
    create_table :collection_items do |t|
      t.float :amount, default: 0.0
      t.date :collection_date
      t.string :reference
      t.references :invoice, foreign_key: true
      t.timestamps
    end
  end
end
