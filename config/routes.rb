require 'sidekiq/web'

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  if Rails.env.development?
    mount Sidekiq::Web => '/sidekiq'
  end

  root 'invoices#index'
  resources :users, only: [:new, :create, :show]
  get 'invoices/sync_invoices', to: 'invoices#sync_invoices', as: :sync_invoices
  resources :invoices, only: [:new, :create, :show]
  resources :collection_item, only: [:new, :create]
end
