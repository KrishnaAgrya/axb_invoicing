require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should create a valid user' do
    @user = User.create(name: 'Hardik', email: 'hardik@gmail.com', password: '12345', password_confirmation: '12345')
    assert_not @user.errors.any?
  end

  test 'should create invaild user' do
    @user = User.create(name: ' ', email: 'hardikgmail.com', password: '12345', password_confirmation: '12345')
    assert_equal error_messages, @user.errors.full_messages
  end

  def error_messages
    [
        "Name can't be blank",
        "Email is invalid",
    ]
  end
end
