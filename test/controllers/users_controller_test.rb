require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:Hardik)
  end

  test 'create new user' do
    get root_path
    get new_user_path
    assert_difference 'User.count', 1 do
      post users_path(user: user_params, format: :js)
    end
  end

  def user_params
    {
        name: 'Sachin',
        email: 'sachin@gmail.com',
        password: '12345',
        password_confirmation: '12345'
    }
  end
end
