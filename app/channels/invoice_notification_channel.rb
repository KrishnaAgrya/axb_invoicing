class InvoiceNotificationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "invoice_notification"
  end
end
