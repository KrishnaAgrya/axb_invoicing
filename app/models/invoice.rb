class Invoice < ApplicationRecord
  has_many :collection_items
  validates :brand_manager, presence: true
  validates :amount, presence: true
  validates :customer_name, presence: true
  validates :invoice_date, presence: true
  validates :narration, presence: true
  validates :reference, presence: true, uniqueness: true

  scope :total_invoices_count, -> {count('*')}

  scope :collected_bills, -> {joins(:collection_items).select('invoices.*')}
  scope :pending_bills, -> {left_joins(:collection_items).where(collection_items: {id: nil}).select('invoices.*')}

  def total_collection_amount
    return collection_items.sum(&:amount) if collection_items.present?
    0
  end

  def balance_due
    amount - total_collection_amount
  end
end
