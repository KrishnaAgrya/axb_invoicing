class CollectionItem < ApplicationRecord
  validates :amount, presence: true
  validates :collection_date, presence: true

  before_create do
    self.invoice_id = Invoice.find_by(reference: reference).id
  end
end
