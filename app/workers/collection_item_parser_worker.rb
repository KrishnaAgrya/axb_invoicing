class CollectionItemParserWorker
  include Sidekiq::Worker

  def perform(collection_item_json)
    @colletion_items = JSON.parse(collection_item_json)
    @colletion_items.each do |ci|
      CollectionItem.create(ci)
    end

    #ActionCable.server.broadcast "invoice_notification", {msg: "#{@colletion_items.count} #{'collection item'.pluralize(@colletion_items.count)} created"}
  end
end
