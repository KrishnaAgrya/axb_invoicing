class InvoiceParserWorker
  include Sidekiq::Worker

  def perform(invoice_json, collection_item_json)
    @invoices = JSON.parse(invoice_json)
    @invoices.each do |inv|
      Invoice.find_or_create_by(inv)
    end

    if collection_item_json.length > 0
      CollectionItemParserWorker.perform_async(collection_item_json)
    end
    
    #ActionCable.server.broadcast "invoice_notification", {msg: "#{@invoices.count} #{'invoice'.pluralize(@invoices.count)} created"}
  end
end
