App.invoice_notification = App.cable.subscriptions.create "InvoiceNotificationChannel",
  received: (data) ->
    if data['msg'].length > 0
      $('[data-id="current-parser-progerss"]').find('p').html(data['msg'])