class UsersController < ApplicationController
  before_action :set_user, only: [:show]

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user.errors.any?
      ''
    else
      ''
    end
  end

  def show
    if @user.authenticate(params[:password])
      ''
    else
      ''
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def set_user
    @user = User.find_by email: params[:email]
    return if @user.present?
    redirect_to request.reffer
  end
end
