class InvoicesController < ApplicationController
  before_action :set_invoice, only: [:show]

  def index
    respond_to do |format|
      format.js do
        if params[:type] == 'nf'
          @invoice = Invoice.new
        elsif params[:type] == 'pb'
          @invoices = Invoice.pending_bills
        elsif params[:type] == 'cb'
          @invoices = Invoice.collected_bills
        elsif params[:type] == 'add_collection'
          @collection = CollectionItem.new
        else
          @invoices = Invoice.all
        end
      end
      format.html {@invoices = Invoice.all}
    end
  end

  def new
    @invoice = Invoice.new
  end

  def create
    @invoice = Invoice.create(invoice_params)
  end

  def show

  end

  def sync_invoices
    collection_item = File.open('collections.json').read
    invoice_file = File.open('new_invoices.json').read
    if invoice_file.length > 0
      InvoiceParserWorker.perform_async(invoice_file, collection_item)
    end
    redirect_to root_path
  end

  private

  def invoice_params
    params.require(:invoice).permit(:brand_manager,
                                    :amount,
                                    :customer_name,
                                    :invoice_date,
                                    :narration,
                                    :reference)
  end

  def set_invoice
    @invoice = Invoice.find_by(id: params[:id])
  end

end
