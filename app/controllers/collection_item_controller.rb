class CollectionItemController < ApplicationController
  def create
    @collction_item = CollectionItem.create(collection_item_params)
  end

  private

  def collection_item_params
    params.require(:collection_item).permit(:amount, :collection_date, :reference)
  end
end
